# Ansible Snippets

A collection of snippets for Ansible files, that I found myself typing too often.
In [VSCode format](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#snippet_syntax)
